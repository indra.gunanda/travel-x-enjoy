@extends('adminlte::page')

@section('title', ((isset($title))?$title:""))

@section('content_header')

@stop

@section('content')
    <div
            class="row"
            style="padding: 20px"
    >
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">{{$title}}</div>
                </div>
                <div class="card-body">
                    <form action="{{($mode === 'show')?"":route($mode === 'create' ?"kk.store":"kk.update",$mode === "edit"?@$penduduk->id:[])}}" method="post">
                        @if($mode === "edit")
                        @method("PUT")
                        @endif
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="number" class="form-control" name="nik" value="{{@$penduduk->nik}}" required />
                                </div>
                                <div class="form-group">
                                    <label>KK</label>
                                    <input type="number" class="form-control" name="kk" value="{{@$penduduk->kk}}" required />
                                </div>
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" class="form-control" name="name" required value="{{@$penduduk->name}}" />
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="jk" id="jk" class="form-control">
                                        @if(@$penduduk->jk === 1)
                                            <option value="1" selected>Laki - Laki</option>
                                            <option value="2">Perempuan</option>
                                        @endif
                                        @if(@$penduduk->jk === 2)
                                            <option value="1">Laki - Laki</option>
                                            <option value="2" selected>Perempuan</option>
                                        @endif
                                        @if(!isset($penduduk->jk))
                                            <option value="1">Laki - Laki</option>
                                            <option value="2" selected>Perempuan</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status Hubungan</label>
                                    <select name="status_hubungan" id="status_hubungan" class="form-control">
                                        @foreach(\App\Casts\StatusHubungan::select(@$penduduk->status_hubungan) as $key => $v)
                                            <option value="{{$v["id"]}}" {{($v["selected"])?"selected":""}}>{{$v["text"]}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Agama</label>
                                    <select name="agama" id="agama" class="form-control">
                                        @foreach(\App\Casts\ListAgama::select(@$penduduk->agama) as $key => $v)
                                            <option value="{{$v["id"]}}" {{($v["selected"])?"selected":""}}>{{$v["text"]}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Pekerjaan</label>
                                    <input type="text" class="form-control" name="pekerjaan" value="{{@$penduduk->pekerjaan}}" required />
                                </div>

                                <div class="form-group">
                                    <label>Jenis Hubungan</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="0" >Kepala Keluarga</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control">{{@$penduduk->alamat}}</textarea>
                                </div>

                            </div>
                            <div class="col-6">


                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" class="form-control" name="tempat_lahir" required value="{{@$penduduk->tempat_lahir}}" />
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" class="form-control date" name="tgl_lahir" value="{{@$penduduk->tgl_lahir}}" required />
                                </div>
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" class="form-control" name="rt" required value="{{@$penduduk->rt}}" />
                                </div>

                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" class="form-control" name="rw" required  value="{{@$penduduk->rw}}" />
                                </div>

                                <div class="form-group">
                                    <label>Desa</label>
                                    <input type="text" class="form-control" name="desa" required  value="{{@$penduduk->desa}}" />
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Datang</label>
                                    <input type="text" class="form-control date" name="tgl_datang" required  value="{{@$penduduk->tgl_datang}}" />
                                </div>
                                @if($mode !== "show")
                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit">
                                            Simpan Data
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>




                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section("js")
    @include("msg")
    <script>
        $(document).ready(function () {
            $("table").dataTable()
            $(".date").datepicker({
                format:'yyyy-mm-dd'
            })
            @if($mode === "show")
                $("form").find("input").attr("disabled",true)
                $("form").find("textarea").attr("disabled",true)
                $("form").find("select").attr("disabled",true)
            @endif
        })
    </script>
@stop
