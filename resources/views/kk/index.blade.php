@extends('adminlte::page')

@section('title', ((isset($title))?$title:""))

@section('content_header')

@stop

@section('content')
    <div
            class="row"
            style="padding: 20px"
    >
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Data Kepala Keluarga</div>
                </div>
                <div class="card-body">
                    <a style="margin-bottom: 10px" href="{{route("kk.create")}}" class="btn btn-success">
                        Tambah
                    </a>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>KK</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Hubungan</th>
                            <th>RT/RW</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($penduduk as $k => $v)
                            <tr>
                                <td>{{($k+1)}}</td>
                                <td>{{$v->kk}}</td>
                                <td>{{$v->nik}}</td>
                                <td>{{$v->name}}</td>
                                <td>{{$v->jk === 1 ? 'Laki - Laki':'Perempuan'}}</td>
                                <td>{{\App\Casts\StatusHubungan::lang($v->status_hubungan)}}</td>
                                <td>{{$v->rt}}/{{$v->rw}}</td>
                                <td>
                                    <a href="{{route("kk.show",$v->id)}}" class="btn btn-primary m-2">Lihat</a>
                                    <a href="{{route("kk.edit",$v->id)}}" class="btn btn-warning m-2">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section("js")
    @include("msg")
    <script>
        $(document).ready(function () {
            $("table").dataTable()
        })
    </script>
@stop
