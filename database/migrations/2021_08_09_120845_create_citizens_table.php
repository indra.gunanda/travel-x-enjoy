<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitizensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizens', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('kk', 100)->nullable();
            $table->string('nik', 100);
            $table->string('name', 100);
            $table->integer('jk');
            $table->integer('status_hubungan');
            $table->text('alamat')->nullable();
            $table->integer('agama');
            $table->string('pekerjaan', 100);
            $table->integer('citizen_id')->nullable()->index('citizen_id');
            $table->integer('type');
            $table->string('tempat_lahir', 100);
            $table->integer('rt');
            $table->integer('rw');
            $table->date('tgl_lahir');
            $table->string('desa', 100);
            $table->date('tgl_datang')->nullable();
            $table->integer('user_id')->index('user_id');
            $table->date('created_at')->nullable();
            $table->date('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizens');
    }
}
