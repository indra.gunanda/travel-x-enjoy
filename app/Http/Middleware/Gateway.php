<?php

namespace App\Http\Middleware;

use App\Casts\LevelAccount;
use App\Casts\ScheduleType;
use App\Casts\StatusAccount;
use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;

class Gateway
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$is_must = null)
    {
        $level = session()->get("level");
        if ($level === NULL || $is_must === NULL){
            if ($request->ajax()){
                return response()->json(["msg"=>"Anda Belum Login "],400);
            }
            return  redirect("/")->withErrors(["msg"=>"Anda Belum Login"]);

        }else{
            $exploded = explode("|",$is_must);

            if (in_array($level,$exploded)){
                $is_must = $level;
//                Config::set("adminlte.sidebar_collapse",true);
                Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e){
                    $e->menu->add([
                        "text"=>"Beranda",
                        "url"=>"dashboard",
                        "icon"=>"fa fa-home"
                    ]);
                });

                if ($level == LevelAccount::ADMIN){
                    Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e){
                        $e->menu->add([
                            "text"=>"Data Penduduk",
                            "url"=>"penduduk",
                            "icon"=>"fa fa-file"
                        ]);
                        $e->menu->add([
                            "text"=>"Data KK",
                            "url"=>"kk",
                            "icon"=>"fa fa-file"
                        ]);
                        $e->menu->add([
                            "text"=>"Transaksi Surat",
                            "url"=>"surat",
                            "icon"=>"fa fa-file"
                        ]);
                    });
                }elseif ($level == LevelAccount::SEKRETARIS){
                    Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e) {

                        $e->menu->add([
                            "text" => "Transaksi Surat",
                            "url" => "surat",
                            "icon" => "fa fa-file"
                        ]);

                        $e->menu->add([
                            "text" => "Laporan",
                            "url" => "laporan",
                            "icon" => "fa fa-file"
                        ]);
                    });
                }elseif ($level == LevelAccount::KEPALA_DESA){
                    Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e){

                        $e->menu->add([
                            "text" => "Transaksi Surat",
                            "url" => "surat",
                            "icon" => "fa fa-file"
                        ]);

                        $e->menu->add([
                            "text" => "Laporan",
                            "url" => "laporan",
                            "icon" => "fa fa-file"
                        ]);

                    });
                }elseif ($level == LevelAccount::WARGA){
                    Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e) {

                        $e->menu->add([
                            "text" => "Pengajuan Surat",
                            "url" => "pengajuan",
                            "icon" => "fa fa-file"
                        ]);

                    });

                }

                Event::listen("JeroenNoten\LaravelAdminLte\Events\BuildingMenu",function ($e){
                    $e->menu->add([
                        "text"=>"Logout",
                        "url"=>"logout",
                        "icon"=>"fa fa-sign-out-alt"
                    ]);

                });
            }

            if ($level == $is_must){
                return $next($request);
            }else{
                if ($request->ajax()){
                    return response()->json(["msg"=>"Anda tidak memiliki akses ke halaman ini  "],400);
                }
                return  redirect("/")->withErrors(["msg"=>"Anda tidak memiliki akses ke halaman ini "]);
            }
        }

    }
}
