<?php

namespace App\Http\Controllers;

use App\Casts\OrderStatus;
use App\Models\TravelCustomer;
use App\Models\TravelOrder;
use App\Traits\ViewTrait;
use Illuminate\Http\Request;
use PDF;
class Laporan extends Controller
{
    use ViewTrait;
    public function __construct()
    {
        $this->base = "laporan";
    }

    public function index()
    {
        $title = "Laporan";
        return $this->loadView('index',compact("title"));
    }

    public function search(Request $req)
    {
        $req->validate([
            "jenis"=>"required",
            "start"=>"required",
            "end"=>"required"
        ]);
        if ($req->jenis == "pelanggan"){
            $cust = TravelCustomer::where(["status"=>1])->whereBetween("created_at",[$req->start,$req->end])->get();
            $start = $req->start;
            $end = $req->end;
            try {
                $pdf = PDF::loadView('template.laporan_pelanggan', compact("start","end","cust"));
                return $pdf->download('laporan_pelanggan.pdf');
            }catch (\Exception $e){
                return view("template.laporan_pelanggan",compact("start","end","cust"));
            }
        }elseif($req->jenis == "tiket"){
            $cust = TravelOrder::where(["status"=>OrderStatus::SELESAI])->whereBetween("created_at",[$req->start,$req->end])->get();
            $start = $req->start;
            $end = $req->end;
            try {
                $pdf = PDF::loadView('template.laporan_penjualan', compact("start","end","cust"));
                return $pdf->download('laporan_pelanggan.pdf');
            }catch (\Exception $e){
                return view("template.laporan_penjualan",compact("start","end","cust"));
            }
        }elseif($req->jenis == "jadwal"){

        }

    }
}
