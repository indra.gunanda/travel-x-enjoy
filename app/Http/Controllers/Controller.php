<?php

namespace App\Http\Controllers;

use http\Env\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function _upload($obj)
    {
        $newFile = md5($obj->getFilename().rand(10,99)).".".$obj->getClientOriginalExtension();
        $temp = $obj->storePubliclyAs("public/product",$newFile);
        $file = str_replace("public/",url("storage")."/",$temp);
        return $file;
    }
}
