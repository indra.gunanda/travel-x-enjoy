<?php

namespace App\Http\Controllers;

use App\Casts\LevelAccount;
use App\Casts\StatusAccount;
use App\Mail\ForgotPassword;
use App\Models\TravelCustomer;
use App\Models\User;
use App\Traits\ViewTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Auth extends Controller
{
    use ViewTrait;
    public function __construct()
    {
        $this->base = "";
    }

    public function index(){
        return view("login");
    }


    public function login(Request $req)
    {
        $req->validate([
            "username"=>"required",
            "password"=>"required"
        ]);

        $cek = User::where(["username"=>$req->username,"password"=>$req->password,"status"=>1]);
        if ($cek->count() > 0){
            $build = [
                "name"=>$cek->first()->name,
                "level"=>$cek->first()->level,
                "id"=>$cek->first()->id,
                "username"=>$cek->first()->username,
                "url"=>"dashboard",
            ];
            session($build);
            return redirect($build["url"])->with(["msg"=>"Selamat Datang ".$build["name"]]);
        }else{
            return back()->withErrors(["msg"=>"Username & Password Salah"]);
        }
    }

    public function logout()
    {
        session()->flush();
        return redirect()->route("login");
    }

}
