<?php

namespace App\Http\Controllers;

use App\Models\Letter;
use Illuminate\Http\Request;

class SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        
        $surat = Letter::latest()->get();
        $title = "Data Surat Keterangan";
        return view('surat.index', compact('surat',"title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $mode = "create";
        $title = "Tambah Data Penduduk";
        return view('surat.form',compact("mode","title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        Letter::create($requestData);

        return redirect('/surat')->with('msg', 'Penduduk added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $surat = Letter::findOrFail($id);
        $mode = "show";
        $title = "Detail Data [$surat->name]";
        return view('surat.form', compact('surat',"mode","title"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $surat = Letter::findOrFail($id);
        $mode = "edit";
        $title = "Edit Data [$surat->name]";
        return view('surat.form', compact('surat',"mode","title"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $surat = Letter::findOrFail($id);
        $surat->update($requestData);

        return redirect('/surat')->with('msg', 'Penduduk updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Letter::destroy($id);

        return redirect('/surat')->with('msg', 'Penduduk deleted!');
    }
}
