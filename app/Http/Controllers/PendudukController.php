<?php

namespace App\Http\Controllers;


use App\Casts\LevelAccount;
use App\Models\Citizen;
use App\Models\User;
use Illuminate\Http\Request;

class PendudukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        if (!empty($keyword)) {
            $penduduk = Citizen::where('kk', 'LIKE', "%$keyword%")
                ->orWhere('nik', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('jk', 'LIKE', "%$keyword%")
                ->orWhere('agama', 'LIKE', "%$keyword%")
                ->orWhere('pekerjaan', 'LIKE', "%$keyword%")
                ->orWhere('alamat', 'LIKE', "%$keyword%")
                ->orWhere('agama', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->orWhere('tempar_lahir', 'LIKE', "%$keyword%")
                ->orWhere('desa', 'LIKE', "%$keyword%")
                ->orWhere('tgl_lahir', 'LIKE', "%$keyword%")
                ->orWhere('tgl_datang', 'LIKE', "%$keyword%")
                ->orWhere('rt', 'LIKE', "%$keyword%")
                ->orWhere('rw', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $penduduk = Citizen::latest()->get();
        }

        $title = "Data Kependudukan";
        return view('penduduk.index', compact('penduduk',"title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $mode = "create";
        $title = "Tambah Data Penduduk";
        return view('penduduk.form',compact("mode","title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $createUser = User::create([
            "name"=>$requestData["name"],
            "username"=>$requestData["nik"],
            "password"=>$requestData["nik"],
            "level"=>LevelAccount::WARGA,
            "status"=>1,
        ]);
        $requestData["user_id"] = $createUser->id;
        Citizen::create($requestData);

        return redirect('/penduduk')->with('msg', 'Penduduk added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $penduduk = Citizen::findOrFail($id);
        $mode = "show";
        $title = "Detail Data [$penduduk->name]";
        return view('penduduk.form', compact('penduduk',"mode","title"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $penduduk = Citizen::findOrFail($id);
        $mode = "edit";
        $title = "Edit Data [$penduduk->name]";
        return view('penduduk.form', compact('penduduk',"mode","title"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $penduduk = Citizen::findOrFail($id);
        $penduduk->update($requestData);

        return redirect('/penduduk')->with('msg', 'Penduduk updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Citizen::destroy($id);

        return redirect('/penduduk')->with('msg', 'Penduduk deleted!');
    }
}
