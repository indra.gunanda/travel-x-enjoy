<?php

namespace App\Http\Controllers;

use App\Casts\LevelAccount;
use App\Models\TravelCustomer;
use App\Models\User;
use App\Traits\ViewTrait;
use Illuminate\Http\Request;

class Akun extends Controller
{
    use ViewTrait;

    public function __construct()
    {
        $this->base = "akun";
    }

    public function index()
    {
        $title = "Data Akun";
        $pelanggan = User::all();
        return $this->loadView("index",compact("title","pelanggan"));
    }

    public function create(Request $req)
    {
        $req->validate([
            "name"=>"required",
            "alamat"=>"required",
            "email"=>"required",
            "no_hp"=>"required",
            "username"=>"required",
            "password"=>"required",
        ]);

        $data = $req->all();
        $data["status"] = 1;
        $create = User::create($data);
        if ($create){
            return back()->with(["msg"=>"Data Berhasil Di Simpan"]);
        }
        return back()->withErrors(["msg"=>"Data Gagal Di Simpan"]);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return back();
    }

    public function update()
    {

    }

}
