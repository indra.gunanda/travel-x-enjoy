<?php

namespace App\Http\Controllers\Landing;

use App\Casts\OrderStatus;
use App\Casts\TransactionStatus;
use App\Http\Controllers\Controller;
use App\Models\TravelCustomer;
use App\Models\TravelOrder;
use App\Models\TravelPackage;
use App\Models\TravelTransaction;
use App\Traits\ViewTrait;
use Illuminate\Http\Request;

class Index extends Controller
{
    use ViewTrait;
    public function __construct()
    {
        $this->base = "landing";
    }

    public function index()
    {
        $title = "Joy Tour Travel";
        $packages = TravelPackage::all();
        return $this->loadView("index",compact("title","packages"));
    }
    public function paket(Request $req)
    {
        $title = "Joy Tour Travel - Paket Tersedia";
        $packages = TravelPackage::where("name","like","%$req->search%")->get();
        return $this->loadView("paket",compact("title","packages"));
    }

    public function panduan()
    {
        $title = "Joy Tour Travel - Pemesanan Paket";
        return $this->loadView("panduan",compact("title"));
    }

    public function detail($id)
    {
        $packages = TravelPackage::find($id);
        $title = "Joy Tour Travel - ".$packages->name;
        return $this->loadView("detail",compact("title","packages"));
    }


    public function booking(Request $req,$id)
    {
        $package = TravelPackage::findOrFail($id);
        $customer_id = session()->get("id");
        if ($customer_id){

            $total = 0;
            $price = $package->price;
            if ($req->has("total")){
                $price = ($req->total * $price);
                $total = $req->total;
            }
            $trx = TravelTransaction::create([
                "travel_package_id"=>$id,
                "travel_voucher_id"=>null,
                "total"=>$price,
                "status"=>TransactionStatus::MENUNGGU_PEMBAYARAN
            ]);
            if ($trx){
                $tc = TravelCustomer::where(["user_id"=>$customer_id]);
                if ($tc->count() > 0){
                    $tc = $tc->first();
                    if ($tc->ktp_number === null || $tc->ktp_img === null){
                        $trx->delete();
                        return redirect()->route("profile.index")->withErrors(["msg"=>"Lengkapi Persyaratan KTP Terlebih Dahulu"]);
                    }
                    $ord = TravelOrder::create([
                        "number"=>$ord_num = "ORD".rand(100,999).rand(10,99),
                        "travel_transaction_id"=>$trx->id,
                        'total'=>$price,
                        'jadwal_keberangkatan'=>$req->jadwal_keberangkatan,
                        "travel_customer_id"=>$tc->id,
                        "status"=>OrderStatus::SEDANG_DIPROSES,
                        "adult"=>$total,
                        "no_chairs"=>0
                    ]);
                    if ($ord){
                        return redirect()->route("tiket.detail",$ord->id)->with(["msg"=>"Order #$ord_num Telah Dibuat"]);
                    }else{
                        $trx->delete();
                    }
                }else{
                    $trx->delete();
                    return back()->withErrors(["msg"=>"Data Pelanggan Tidak Ditemukan $customer_id"]);
                }

            }
        }
        return redirect()->route("login")->withErrors(["msg"=>"Anda Harus Login Terlebih Dahulu"]);
    }

}
