<?php

namespace App\Casts;

class JenisSurat
{
    const  KET_LAHIR = 0;
    const  KET_PINDAH = 1;
    const  KET_DATANG = 2;
    const  KET_MATI = 3;
    const  KET_USAHA = 4;
    const  KET_KERAMAIAN = 5;
    const  KET_SKTM = 6;
    const  KET_SKCK = 7;


    public static function lang($level)
    {
        if ($level == self::KET_LAHIR){
            return "Surat Keterangan Lahir";
        }elseif ($level == self::KET_DATANG){
            return "Surat Keterangan Datang";
        }elseif ($level == self::KET_MATI){
            return "Surat Keterangan Kematian";
        }elseif ($level == self::KET_PINDAH){
            return "Surat Keterangan Pindah";
        }elseif ($level == self::KET_USAHA){
            return "Surat Keterangan Usaha";
        }elseif ($level == self::KET_KERAMAIAN){
            return "Surat Keterangan Keramaian";
        }elseif ($level == self::KET_SKTM){
            return "Surat Keterangan Tidak Mampu";
        }elseif ($level == self::KET_SKCK){
            return "Surat Pengantar Cacatan Kepolisian";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 0; $i <= 7; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }
}