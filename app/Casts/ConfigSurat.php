<?php

namespace App\Casts;

class ConfigSurat
{
    /**
     * @param $tgl_meninggal
     * @param $sebab_meninggal
     * @return array
     */
    public static function SK_KEMATIAN($tgl_meninggal,$sebab_meninggal)
    {
        return [
            "tgl_meninggal"=>$tgl_meninggal,
            "sebab_meninggal"=>$sebab_meninggal
        ];
    }

    /**
     * @param $nama
     * @param $tgl_lahir
     * @param $jk
     * @return array
     */
    public static function SK_KELAHIRAN($nama,$tgl_lahir,$jk)
    {
        return [
            "nama"=>$nama,
            "tgl_lahir"=>$tgl_lahir,
            "jk"=>$jk
        ];
    }

    /**
     * @param $kk
     * @param $nik
     * @param $nama
     * @param $jk
     * @param $pekerjaan
     * @param $alamat
     * @param $tgl_datang
     * @return array
     */
    public static function SK_DATANG($kk,$nik,$nama,$jk,$pekerjaan,$alamat,$tgl_datang)
    {
        return compact("kk","nik","nama","jk","pekerjaan","alamat","tgl_datang");
    }

    /**
     * @param $kk
     * @param $nik
     * @param $nama
     * @param $jk
     * @param $pekerjaan
     * @param $alamat
     * @param $alasan
     * @return array
     */
    public static function SK_PINDAH($kk,$nik,$nama,$jk,$pekerjaan,$alamat,$alasan)
    {
        return compact("kk","nik","nama","jk","pekerjaan","alamat","alasan");
    }

    /**
     * @param $nama_usaha
     * @param $alamat_usaha
     * @return array
     */
    public static function SK_USAHA($nama_usaha,$alamat_usaha)
    {
        return compact("nama_usaha","alamat_usaha");
    }


    /**
     * @param $tgl
     * @param $jam
     * @param $tempat
     * @param $nama_acara
     * @return array
     */
    public static function SK_KERAMAIAN($tgl,$jam,$tempat,$nama_acara)
    {
        return compact("tgl","jam","tempat","nama_acara");
    }

}