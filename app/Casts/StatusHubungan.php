<?php

namespace App\Casts;

class StatusHubungan
{
    const  BELUM_KAWIN = 0;
    const  KAWIN = 1;
    const  CERAI = 2;


    public static function lang($level)
    {
        if ($level == self::BELUM_KAWIN){
            return "Belum Kawin";
        }elseif ($level == self::KAWIN){
            return "Kawin";
        }elseif ($level == self::CERAI){
            return "Cerai";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 0; $i <= 2; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }
}