<?php

namespace App\Casts;


class LevelAccount
{
    const  ADMIN = 0;
    const  WARGA = 1;
    const  SEKRETARIS = 2;
    const  KEPALA_DESA = 3;


    public static function lang($level)
    {
        if ($level == self::ADMIN){
            return "Admninistrator";
        }elseif ($level == self::WARGA){
            return "Warga";
        }elseif ($level == self::SEKRETARIS){
            return "Sekretaris";
        }elseif ($level == self::KEPALA_DESA){
            return "Kepala Desa";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 0; $i <= 3; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }

}
