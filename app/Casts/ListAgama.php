<?php

namespace App\Casts;

class ListAgama
{
    const  ISLAM = 0;
    const  KRISTEN = 1;
    const  BUDHA = 2;
    const  HINDU = 3;
    const  KONGHUCHU = 4;
    const  KATOLIK = 5;


    public static function lang($level)
    {
        if ($level == self::ISLAM){
            return "Islam";
        }elseif ($level == self::KRISTEN){
            return "Kristen";
        }elseif ($level == self::BUDHA){
            return "Buddha";
        }elseif ($level == self::KONGHUCHU){
            return "Konghuchu";
        }elseif ($level == self::KATOLIK){
            return "Katolik";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 0; $i <= 5; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }
}