<?php

namespace App\Casts;

class CitizenType
{
    const  KEPALA_KELUARGA = 0;
    const  ISTRI = 1;
    const  ANAK = 2;
    const  ORANG_TUA = 3;
    const  MERTUA = 4;
    const  MENANTU = 5;
    const  CUCU = 6;


    public static function lang($level)
    {
        if ($level == self::KEPALA_KELUARGA){
            return "Kepala Keluarga";
        }elseif ($level == self::ISTRI){
            return "Istri";
        }elseif ($level == self::ANAK){
            return "Anak";
        }elseif ($level == self::ORANG_TUA){
            return "Orang Tua";
        }elseif ($level == self::MENANTU){
            return "Menantu";
        }elseif ($level == self::CUCU){
            return "Cucu";
        }elseif ($level == self::MERTUA){
            return "Mertua";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 1; $i <= 6; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }
}