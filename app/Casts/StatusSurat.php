<?php

namespace App\Casts;

class StatusSurat
{
    const  MENUNGGU_PERSETUJUAN = 0;
    const  SETUJU = 1;
    const  DITOLAK = 2;


    public static function lang($level)
    {
        if ($level == self::MENUNGGU_PERSETUJUAN){
            return "Menunggu Persetujuan";
        }elseif ($level == self::SETUJU){
            return "Disetujui";
        }elseif ($level == self::DITOLAK){
            return "Ditolak";
        }else{
            return  FALSE;
        }
    }

    public static function select($level)
    {
        $select = [];
        for ($i = 0; $i <= 2; $i++){
            $select[] = [
                "id"=>$i,
                "text"=>self::lang($i),
                "selected"=>($level == $i)
            ];
        }
        return $select;
    }
}