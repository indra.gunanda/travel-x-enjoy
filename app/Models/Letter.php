<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Letter
 * 
 * @property int $id
 * @property int $type
 * @property string $bukti_pendukung
 * @property array|null $config
 * @property int $citizen_id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Citizen $citizen
 * @property User $user
 *
 * @package App\Models
 */
class Letter extends Model
{
	protected $table = 'letters';

	protected $casts = [
		'type' => 'int',
		'config' => 'json',
		'citizen_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'type',
		'bukti_pendukung',
		'config',
		'citizen_id',
		'user_id'
	];

	public function citizen()
	{
		return $this->belongsTo(Citizen::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
