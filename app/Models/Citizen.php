<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Citizen
 * 
 * @property int $id
 * @property string|null $kk
 * @property string $nik
 * @property string $name
 * @property int $jk
 * @property int $status_hubungan
 * @property string|null $alamat
 * @property int $agama
 * @property string $pekerjaan
 * @property int|null $citizen_id
 * @property int $type
 * @property string $tempat_lahir
 * @property int $rt
 * @property int $rw
 * @property Carbon $tgl_lahir
 * @property string $desa
 * @property Carbon|null $tgl_datang
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property User $user
 * @property Citizen $citizen
 * @property Collection|Citizen[] $citizens
 * @property Collection|Letter[] $letters
 *
 * @package App\Models
 */
class Citizen extends Model
{
	protected $table = 'citizens';

	protected $casts = [
		'jk' => 'int',
		'status_hubungan' => 'int',
		'agama' => 'int',
		'citizen_id' => 'int',
		'type' => 'int',
		'rt' => 'int',
		'rw' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'tgl_lahir',
		'tgl_datang'
	];

	protected $fillable = [
		'kk',
		'nik',
		'name',
		'jk',
		'status_hubungan',
		'alamat',
		'agama',
		'pekerjaan',
		'citizen_id',
		'type',
		'tempat_lahir',
		'rt',
		'rw',
		'tgl_lahir',
		'desa',
		'tgl_datang',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function citizen()
	{
		return $this->belongsTo(Citizen::class);
	}

	public function citizens()
	{
		return $this->hasMany(Citizen::class);
	}

	public function letters()
	{
		return $this->hasMany(Letter::class);
	}
}
