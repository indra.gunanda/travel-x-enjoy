<?php

use App\Http\Controllers\Auth;
use App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/login",[Auth::class,"index"])->name("login");
Route::get("/",function (){
    return redirect()->route("login");
});
Route::post("/login",[Auth::class,"login"])->name("login.post");
Route::get("/logout",[Auth::class,"logout"])->name("logout");


Route::get("/dashboard",[Dashboard::class,"index"])->middleware("gateway:0|1|2|3|4|5")->name("dashboard");

Route::resource('penduduk', 'PendudukController')->middleware("gateway:0");
Route::resource('kk', 'KkController')->middleware("gateway:0");
Route::resource('surat', 'SuratController')->middleware("gateway:0");